import logging
import os

from datetime import timedelta

from discord import app_commands, Intents, Message, Member, Interaction, Object
from discord.ext import commands

from django.conf import settings
from django.utils.translation import gettext as _

from cog_hangman.cog import HangmanCog
from cog_image.cog import ImageCog
from cog_junk.cog import JunkCog
from cog_poll.cog import PollCog
from cog_tictactoe.cog import TicTacToeCog

logger = logging.getLogger('default')


class LiamBot(commands.Bot):
    """ Client for LIAM Discord Bot. """

    def __init__(self, *, intents: Intents, application_id: int):
        super().__init__('>', intents=intents, application_id=application_id)

    async def setup_hook(self) -> None:
        # register cogs
        await bot.add_cog(HangmanCog(bot))
        await bot.add_cog(ImageCog(bot))
        await bot.add_cog(JunkCog(bot))
        await bot.add_cog(TicTacToeCog(bot))
        await bot.add_cog(PollCog(bot))

        if settings.DEBUG:
            # synchronise app commands to Discord
            self.tree.copy_global_to(guild=Object(id=os.environ.get('TEST_GUILD_ID')))
            await self.tree.sync(guild=Object(id=os.environ.get('TEST_GUILD_ID')))
        else:
            await self.tree.sync()


intents = Intents.default()
bot = LiamBot(intents=intents, application_id=os.environ.get('APPLICATION_ID'))


@bot.event
async def on_ready() -> None:
    logger.info(_('LIAM_LOGGED_IN_LOG') % {'user': bot.user, 'id': bot.user.id})


@bot.tree.context_menu()
async def ratio(interaction: Interaction, message: Message) -> None:
    await interaction.response.send_message(_('LIAM_RATIO_MESSAGE'), ephemeral=True)
    await message.add_reaction('🇷')
    await message.add_reaction('🇦')
    await message.add_reaction('🇹')
    await message.add_reaction('🇮')
    await message.add_reaction('🇴')
    logger.info(_('LIAM_RATIO_LOG') % {'user': interaction.user, 'target': message.author})
