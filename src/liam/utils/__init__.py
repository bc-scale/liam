import json
import random
import requests

from bs4 import BeautifulSoup

from django.conf import settings

from urllib.parse import quote


def randchar(c: str) -> str:
    """Replace randomly a char to cancerize a string"""
    # nosemgrep
    if random.random() > 0.5: # nosec
        c = c.upper()
        # nosemgrep
        if c == 'E' and random.random() < 0.25: # nosec
            return '3'
        # nosemgrep
        if c == 'O' and random.random() < 0.4: # nosec
            return '0'
        # nosemgrep
        if c == 'S' and random.random() < 0.2: # nosec
            return '$'
        return c
    return c.lower()


def get_slogan(word: str) -> str:
    """Get a random slogan"""
    response = requests.get(settings.URL_SLOGAN.format(quote(word)))
    soup = BeautifulSoup(response.text, 'html.parser')
    slogan = [mantra.text for mantra in soup.find_all("span", "business-name")][0]
    return slogan


def get_cat() -> str:
    """Get a random cat from API"""
    return requests.get(settings.URL_CAT).json()[0]['url'] 
