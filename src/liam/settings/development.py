"""
Configuration for local development
"""

from liam.settings.base import *

from dotenv import load_dotenv

load_dotenv(verbose=True, override=True, dotenv_path='../liam.env')

SECRET_KEY = os.environ.get('TOKEN_DJANGO')
TOKEN_DISCORD = os.environ.get('TOKEN_DISCORD')

DEBUG = True

INSTALLED_APPS.append('debug_toolbar')
MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']
INTERNAL_IPS = ["127.0.0.1"]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'dev/db.sqlite3')
    }
}

LOG_DIRECTORY = BASE_DIR / 'dev/'
if not os.path.isdir(LOG_DIRECTORY):
    os.mkdir(LOG_DIRECTORY)

# pylint: disable=duplicate-code
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '[{asctime}][{levelname}] {message}',
            'datefmt' : '%d/%m/%Y %H:%M:%S',
            'style': '{',
        },
        'verbose': {
            'format': '[{asctime}][{levelname}] {module} {process:d} {thread:d} {message}',
            'datefmt' : '%d/%m/%Y %H:%M:%S',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'file': {
            'class': 'logging.FileHandler',
            'filename': LOG_DIRECTORY / 'liam.log',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'default': {
            'handlers': ['console', 'file'],
            'level': os.getenv('DEFAULT_LOG_LEVEL', 'INFO'),
        },
    },
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR / 'dev/static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR / 'dev/media/'
