"""
Configuration for production environment
"""

from liam.settings.base import *

from dotenv import load_dotenv

SECRET_KEY = os.environ.get('TOKEN_DJANGO')
TOKEN_DISCORD = os.environ.get('TOKEN_DISCORD')

DEBUG = bool(os.getenv('DEBUG', False))

ALLOWED_HOSTS = [os.environ.get('PRODUCTION_DOMAIN')]
CSRF_TRUSTED_ORIGINS = ['https://' + os.environ.get('PRODUCTION_DOMAIN')]

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.postgresql_psycopg2',
        'NAME':     os.environ.get('POSTGRES_DB'),
        'USER':     os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST':     os.environ.get('POSTGRES_HOST'),
        'PORT':     os.environ.get('POSTGRES_PORT'),
    }
}

LOG_DIRECTORY = '/code/files/logs/'

# pylint: disable=duplicate-code
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '[{asctime}][{levelname}] {message}',
            'datefmt' : '%d/%m/%Y %H:%M:%S',
            'style': '{',
        },
        'verbose': {
            'format': '[{asctime}][{levelname}] {module} {process:d} {thread:d} {message}',
            'datefmt' : '%d/%m/%Y %H:%M:%S',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'file': {
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIRECTORY, 'liam.log'),
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'default': {
            'handlers': ['console', 'file'],
            'level': os.getenv('DEFAULT_LOG_LEVEL', 'INFO'),
        },
    },
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '/code/files/static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = '/code/files/media/'
