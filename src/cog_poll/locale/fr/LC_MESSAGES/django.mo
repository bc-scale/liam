��    %      D  5   l      @      A     b     �     �     �     �     �     �     �               3     P     h     �     �     �     �     �     �               1     B     X     n     �     �     �     �     �     �     �               +  ;  H  (   �  I   �  %   �          .     G     b     y  )   �     �  %   �     �      �     	  %    	     F	  ,   W	     �	  '   �	     �	  )   �	     	
     !
  )   ;
     e
     i
     �
     �
     �
     �
  "   �
  $   �
     $     B     J     _                                                       $                              %                   "                        
          	                   !         #             POLL_ADD_CHOICE_ERROR_DUPPLICATE POLL_ADD_CHOICE_ERROR_TOO_LONG POLL_ADD_CHOICE_ERROR_TOO_MANY POLL_ADD_LABEL POLL_AUTHOR_FOOTER POLL_AUTHOR_LABEL POLL_CANCEL POLL_CANCEL_LABEL POLL_CLOSE_ERROR POLL_CLOSE_LABEL POLL_DELETE_CHOICE_PLACEHOLDER POLL_DESCRIPTION_MODAL_LABEL POLL_EDIT_CHOICES_ERROR POLL_EDIT_CHOICES_LABEL POLL_EDIT_CHOICES_LABEL_AWARE POLL_EDIT_CHOICES_MODAL_TITLE POLL_EDIT_ERROR POLL_EDIT_NAME_LABEL POLL_LOG_CLOSE_VOTE POLL_LOG_START_CREATE POLL_LOG_START_VOTE POLL_MULTI_FOOTER POLL_MULTI_LABEL POLL_MULTI_VOTE_ERROR POLL_NAME_MODAL_LABEL POLL_NAME_MODAL_TITLE POLL_NEW_CHOICE_LABEL POLL_NEW_CHOICE_MODAL_TITLE POLL_PUBLIC_FOOTER POLL_PUBLIC_LABEL POLL_TEMP_FOOTER POLL_TEMP_LABEL POLL_VALIDATED POLL_VALIDATE_LABEL POLL_VOTERS POLL_VOTE_CHOICE_PLACEHOLDER Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-07-03 15:48+2
Last-Translator: Imotekh <imotekh@protonmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Le choix "%(value)s" apparaît en double La taille du choix ne doit pas dépasser 100 caractères (ici %(length)s) tu ne peux pas avoir plus de 25 choix Ajouter un choix %(show)s Afficher auteur Afficher auteur : %(show)s Le sondage est annulé Annuler Tu n'es pas autorisé à clore le sondage Clore le sondage Sélectionne les options à supprimer Description Il n'y a pas de choix à éditer Editer les choix Editer les choix %(start)s à %(end)s Editer les choix Tu n'est pas autorisé à éditer le sondage Editer Nom / Description %(author)s a clos le sondage "%(name)s" %(user)s crée un sondage %(author)s a lancé le sondage "%(name)s" %(show)s Choix multiple Choix multiple : %(show)s Tu n'es pas autorisé à clore le sondage Nom Editer le nom et la description Nouveau choix Ajouter un nouveau choix %(show)s Vote public Vote public : %(show)s %(show)s Résultats intermédiares Résultats intermédiares : %(show)s Le sondage est déjà validé Valider Votants : %(voters)s Sélectionne tes choix 