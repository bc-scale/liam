import logging
import random

from asgiref.sync import sync_to_async

from discord import app_commands, errors, Interaction, Message, MessageType
from discord.ext import commands

from django.conf import settings
from django.utils.translation import gettext as _

from cog_junk.models import Reply
from liam.utils import get_cat, get_slogan, randchar

logger = logging.getLogger('default')

class JunkCog(commands.Cog):

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    GREETINGS = ['bonjour', 'bjr', 'slt', 'yo', 'cc', 'coucou', 'hello']
    QUOI_FEUR = ["quoi", "coi", "koa", "koi"]
    STRING_CANER = "Mes aievx, qvelle fort belle jovrnee povr ressentir en mien coevr vne si belle envie de caner"

    @app_commands.command()
    async def fight(self, interaction: Interaction) -> None:
        """ (ง'̀-'́)ง """
        logger.info(_('JUNK_LOG_FIGHT') % {'user': interaction.user})
        await interaction.response.send_message("(ง'̀-'́)ง")

    @app_commands.command()
    async def caner(self, interaction: Interaction) -> None:
        """ Caner. """
        logger.info(_('JUNK_LOG_CANER'))
        await interaction.response.send_message(self.STRING_CANER)

    @app_commands.command()
    async def cat(self, interaction: Interaction) -> None:
        """ Awww. """
        url = get_cat()
        await interaction.response.send_message(url)
        logger.info(_('JUNK_LOG_CAT') % {'user': interaction.user, 'url': url})

    @app_commands.command()
    async def slogan(self, interaction: Interaction, expression: str) -> None:
        """ Return a random slogan with the provided expression. """
        logger.info(_('JUNK_LOG_SLOGAN') % {'user': interaction.user, 'expression': expression})
        await interaction.response.send_message(get_slogan(expression))

    @app_commands.command()
    async def cancer(self, interaction: Interaction, expression: str) -> None:
        """ Return a cancerized string. """
        logger.info(_('JUNK_LOG_CANCER') % {'user': interaction.user, 'expression': expression})
        await interaction.response.send_message(''.join(map(randchar, expression)))

    @app_commands.command()
    async def no_ref(self, interaction: Interaction) -> None:
        """ Return a string when not catching the joke. """
        logger.info(_('JUNK_LOG_NO_REF') % {'user': interaction.user})
        no_ref_string = settings.NO_REF % {'joke': _('JUNK_NO_REF_JOKE'), 'me': _('JUNK_NO_REF_ME')}
        await interaction.response.send_message(no_ref_string)

    @commands.Cog.listener()
    async def on_message(self, message: Message) -> None:
        if message.type != MessageType.chat_input_command:
            try:
                message = await message.fetch()
            except errors.NotFound:
                return
            content = message.content.lower().replace('?', '').replace('.', '').replace(',', '')
            word_array = content.split(' ')

            if not message.author.bot:
                reply = await sync_to_async(Reply.objects.get_random)()
                for user in message.mentions:
                    if reply and user.id == self.bot.user.id:
                        await message.channel.send(reply.message)

                if "liam" in word_array:
                    await message.channel.send(get_slogan("Liam"))
                if "rip" in word_array:
                    await message.add_reaction('🪦')

            if any(txt in word_array for txt in self.GREETINGS):
                await message.add_reaction('👋')
                logger.info(_('JUNK_LOG_GREETINGS') % {'user': message.author})

            content = content.replace(' ', '')
            if any(content.endswith(suffix) for suffix in self.QUOI_FEUR) and random.random() < 0.33:
                await message.add_reaction('🇫')
                await message.add_reaction('🇪')
                await message.add_reaction('🇺')
                await message.add_reaction('🇷')
                logger.info(_('JUNK_LOG_QUOI_FEUR') % {'user': message.author})
