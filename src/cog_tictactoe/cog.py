import logging

from discord import app_commands, Member, Interaction, ButtonStyle
from discord.ext import commands
from discord.ui import View, Button

from django.utils.translation import gettext as _

from typing import List

logger = logging.getLogger('default')

class TicTacToeButton(Button['TicTacToe']):
    def __init__(self, x: int, y: int):
        super().__init__(style=ButtonStyle.secondary, label='\u200b', row=y)
        self.x = x
        self.y = y

    async def callback(self, interaction: Interaction)  -> None:
        if self.view is None:
            return
        view: TicTacToe = self.view
        state = view.board[self.y][self.x]
        if state in (view.X, view.O):
            return

        content = ''
        if view.current_player == view.X:
            content = _('TICTACTOE_ROUND_PLAYER') % {'player': self.view.player1.mention}
            if interaction.user == self.view.player1:
                self.style = ButtonStyle.danger
                self.label = 'X'
                self.disabled = True
                view.board[self.y][self.x] = view.X
                view.current_player = view.O
                content = _('TICTACTOE_ROUND_PLAYER') % {'player': self.view.player2.mention}
        elif view.current_player == view.O:
            content = _('TICTACTOE_ROUND_PLAYER') % {'player': self.view.player2.mention}
            if interaction.user == self.view.player2:
                self.style = ButtonStyle.success
                self.label = 'O'
                self.disabled = True
                view.board[self.y][self.x] = view.O
                view.current_player = view.X
                content = _('TICTACTOE_ROUND_PLAYER') % {'player': self.view.player1.mention}

        winner = view.check_board_winner()
        if winner is not None:
            if winner == view.X:
                content = _('TICTACTOE_WINNER') % {'player': self.view.player1.mention}
                logger.info(_('TICTACTOE_LOG_WINNER') % {'winner': self.view.player1, 'loser': self.view.player2})
            elif winner == view.O:
                content = _('TICTACTOE_WINNER') % {'player': self.view.player2.mention}
                logger.info(_('TICTACTOE_LOG_WINNER') % {'winner': self.view.player2, 'loser': self.view.player1})
            else:
                content = _('TICTACTOE_TIE')
                logger.info(_('TICTACTOE_LOG_TIE') % {'p1': self.view.player1, 'p2': self.view.player2})

            for child in view.children:
                child.disabled = True

            view.stop()

        await interaction.response.edit_message(content=content, view=view)

class TicTacToe(View):

    children: List[TicTacToeButton]
    X = -1
    O = 1
    Tie = 2

    player1: Member
    player2: Member

    def __init__(self, player1: Member, player2: Member):
        super().__init__()
        self.player1 = player1
        self.player2 = player2
        self.current_player = self.X
        self.board = [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
        ]

        # fill grid
        for x in range(3):
            for y in range(3):
                self.add_item(TicTacToeButton(x, y))
        logger.info(_('TICTACTOE_LOG_START') % {'p1': self.player1, 'p2': self.player2})

    def check_board_winner(self) -> int:
        # check horizontal
        for across in self.board:
            value = sum(across)
            if value == 3:
                return self.O
            elif value == -3:
                return self.X

        # check vertical
        for line in range(3):
            value = self.board[0][line] + self.board[1][line] + self.board[2][line]
            if value == 3:
                return self.O
            elif value == -3:
                return self.X

        # check diagonals
        diag = self.board[0][2] + self.board[1][1] + self.board[2][0]
        if diag == 3:
            return self.O
        elif diag == -3:
            return self.X

        diag = self.board[0][0] + self.board[1][1] + self.board[2][2]
        if diag == 3:
            return self.O
        elif diag == -3:
            return self.X

        # if we're here, we need to check if a tie was made
        if all(i != 0 for row in self.board for i in row):
            return self.Tie

        return None

class TicTacToeCog(commands.Cog):

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @app_commands.command()
    async def tictactoe(self, interaction: Interaction, player2: Member) -> None:
        if player2.bot:
            logger.info(_('TICTACTOE_LOG_PLAYER2_BOT') % {'user': interaction.user})
            await interaction.response.send_message(content=_('TICTACTOE_PLAYER2_BOT'), ephemeral=True)
        else:
            await interaction.response.send_message(
                content= _('TICTACTOE_ROUND_PLAYER') % {'player': interaction.user.mention},
                view=TicTacToe(player1=interaction.user, player2=player2)
            )
