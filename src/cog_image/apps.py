from django.apps import AppConfig

class CogImageConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cog_image'
