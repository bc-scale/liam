��          |      �             !     ?     T     p     �     �     �     �     �     �       ;  #  $   _  )   �  i   �  V     Y   o  <   �  J        Q     m  !   �  %   �                            	       
                        IMAGE_CREATE_FAIL_TO_DOWNLOAD IMAGE_CREATE_SUCCESS IMAGE_LOG_ADD_IMAGE_TO_POOL IMAGE_LOG_CREATE IMAGE_LOG_DELETE IMAGE_LOG_SEND_IMAGE_FROM_POOL IMAGE_LOG_UNKNOWN_OR_EMPY_POOL IMAGE_POOL_ALREADY_EXISTS IMAGE_POOL_CREATED IMAGE_POOL_DELETED IMAGE_UNKNOWN_POOL Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-07-03 15:48+2
Last-Translator: Imotekh <imotekh@protonmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Echec du téléchargement de l'image L'image a été ajoutée à la catégorie %(user)s a essayé d'ajouter l'image %(url)s à la catégorie '%(pool)s' avec le résultat : %(response)s %(user)s a essayé de créer la catégorie '%(pool)s' avec le résultat : %(response)s %(user)s a essayé de supprimer la catégorie '%(pool)s' avec le résultat : %(response)s %(user)s a reçu l'image %(url)s de la catégorie '%(pool)s' %(user)s a demandé une image de la catégorie inconnue ou vide '%(pool)s' La catégorie existe déjà La catégorie a été créée La cétégorie a été supprimée La catégorie '%(pool)s' est inconnue 