from django.apps import AppConfig

class CogHangmanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cog_hangman'
