import logging
import unidecode

from discord import app_commands, errors, Interaction, Message, Embed, Member
from discord.ext import commands

from django.conf import settings
from django.utils.translation import gettext as _

from typing import List

logger = logging.getLogger('default')

class HangmanGame():

    author: Member
    message: Message
    to_guess: str
    letters: List[str]
    words: List[str]
    lifepoints: int
    ongoing: bool

    def __init__(self, to_guess: str, message: Message, author: Member):
        self.author = author
        self.message = message
        self.to_guess = to_guess
        self.letters = []
        self.words = []
        self.lifepoints = len(settings.HANGMAN) -1
        self.ongoing = True
        self.description = _('HANGMAN_STATUS_DEFAULT')

    def _lose_lifepoint(self) -> None:
        self.lifepoints -= 1
        if self.lifepoints == 0:
            self.description = _('HANGMAN_STATUS_LOSE')
            self.ongoing = False

    def test_letter(self, letter, author) -> Embed:

        if not self.ongoing:
            logger.info(_('HANGMAN_LOG_STATUS_FINISH') % {'user': author})
            return Embed(title=_('HANGMAN_STATUS_FINISH'), color=0xa71e00)
        if self.author == author:
            logger.info(_('HANGMAN_LOG_OWNER_CANT_PLAY') % {'user': author})
            return Embed(title=_('HANGMAN_OWNER_CANT_PLAY'), color=0xa71e00)
        if len(letter) != 1 or letter in self.letters or not letter.isalpha():
            logger.info(_('HANGMAN_LOG_INVALID_LETTER') % {'user': author, 'letter': letter})
            return Embed(title=_('HANGMAN_INVALID_LETTER') % {'letter': letter}, color=0xa71e00)

        self.letters.append(letter)
        if letter not in self.to_guess:
            self._lose_lifepoint()
            logger.info(_('HANGMAN_LOG_LETTER_IS_NOT_PRESENT') % {'user': author, 'letter': letter})
            return Embed(title=_('HANGMAN_LETTER_IS_NOT_PRESENT') % {'letter': letter}, color=0xa71e00)
        else:
            logger.info(_('HANGMAN_LOG_LETTER_IS_PRESENT') % {'user': author, 'letter': letter})
            if all(character in self.letters for character in self.to_guess):
                logger.info(_('HANGMAN_LOG_LETTER_ALL_FOUND') % {'user': author, 'word': self.to_guess})
                self.description = _('HANGMAN_STATUS_WINNER') % {'winner': author.mention}
                self.ongoing = False
                return Embed(title=_('HANGMAN_LETTER_ALL_FOUND') % {'word': self.to_guess}, color=0x23a700)
            else:
                return Embed(title=_('HANGMAN_LETTER_IS_PRESENT') % {'letter': letter}, color=0x23a700)

    def test_word(self, word, author) -> Embed:

        if not self.ongoing:
            logger.info(_('HANGMAN_LOG_STATUS_FINISH') % {'user': author})
            return Embed(title=_('HANGMAN_STATUS_FINISH'), color=0xa71e00)
        if self.author == author:
            logger.info(_('HANGMAN_LOG_OWNER_CANT_PLAY') % {'user': author})
            return Embed(title=_('HANGMAN_OWNER_CANT_PLAY'), color=0xa71e00)

        if word in self.words:
            logger.info(_('HANGMAN_LOG_INVALID_WORD') % {'user': author, 'word': word})
            return Embed(title=_('HANGMAN_INVALID_WORD') % {'word': word}, color=0xa71e00)

        self.words.append(word)
        if word == self.to_guess:
            logger.info(_('HANGMAN_LOG_STATUS_WINNER') % {'user': author, 'word': word})
            self.description = _('HANGMAN_STATUS_WINNER') % {'winner': author.mention}
            self.ongoing = False
            return Embed(title=_('HANGMAN_CORRECT_WORD'), color=0x23a700)
        else:
            self._lose_lifepoint()
            logger.info(_('HANGMAN_LOG_WRONG_WORD') % {'user': author, 'word': word})
            return Embed(title=_('HANGMAN_WRONG_WORD') % {'word': word}, color=0xa71e00)

    def stop(self, author) -> Embed:
        if self.author == author:
            self.ongoing = False
            self.description = _('HANGMAN_STATUS_STOPPED')
            logger.info(_('HANGMAN_LOG_STATUS_STOPPED') % {'user': author})
            return Embed(title=_('HANGMAN_STATUS_STOPPED'), color=0x23a700)
        else:
            logger.info(_('HANGMAN_LOG_CANT_STOP_GAME_NOT_OWNED') % {'user': author})
            return Embed(title=_('HANGMAN_CANT_STOP_GAME_NOT_OWNED'), color=0xa71e00)

    @property
    def guessing_string(self) -> str:
        guess_string = ''
        for letter in self.to_guess:
            if not letter.isalpha():
                guess_string += letter + ' '
            elif letter in self.letters or not self.ongoing:
                guess_string += letter + ' '
            else:
                guess_string += '_ '
        return f'```{guess_string}```'

    @property
    def sketch(self) -> str:
        return f'```{settings.HANGMAN[len(settings.HANGMAN) - self.lifepoints -1]}```'
    
    async def draw(self) -> None:
        embed = Embed(description=self.description, color=0xdbb706)
        if self.author.nick:
            embed.set_author(name=self.author.nick, icon_url=self.author.display_avatar.url)
        else:
            embed.set_author(name=self.author.name, icon_url=self.author.display_avatar.url)
        embed.add_field(name=self.guessing_string, value=self.sketch)
        if self.letters:
            embed.add_field(name=_('HANGMAN_FIELD_LETTERS'), value=', '.join(self.letters), inline=True)
        if self.words:
            embed.add_field(name=_('HANGMAN_FIELD_WORDS'), value=', '.join(self.words), inline=True)
        try:
            if not self.message:
                self.message = await self.message.channel.send(embed=embed)
            else:
                self.message = await self.message.edit(content='', embed=embed)
        except errors.NotFound:
            self.message = await self.message.channel.send(embed=embed)

class HangmanCog(commands.Cog):

    games: List[HangmanGame]

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.games = []

    def find_game(self, channel) -> HangmanGame:
        found_game = None
        for game in self.games:
            if game.message.channel == channel and game.ongoing:
                found_game = game
                break
        return found_game

    async def hangman_action(self, interaction, callback) -> None:
        game = self.find_game(interaction.channel)
        if game:
            await interaction.response.send_message(embed=callback(game), ephemeral=True)
            await game.draw()
        else:
            logger.info(_('HANGMAN_LOG_NO_GAME_ON_THIS_CHANNEL') % {'user': interaction.user})
            embed = Embed(title=_('HANGMAN_NO_GAME_ON_THIS_CHANNEL'), color=0xa71e00)
            await interaction.response.send_message(embed=embed, ephemeral=True)

    @app_commands.command()
    async def hangman(self, interaction: Interaction, to_guess: str) -> None:

        game = self.find_game(interaction.channel)
        if game:
            logger.info(_('HANGMAN_LOG_ALREADY_A_GAME_ON_THIS_CHANNEL') % {'user': interaction.user})
            embed = Embed(title=_('HANGMAN_ALREADY_A_GAME_ON_THIS_CHANNEL'), color=0xa71e00)
            await interaction.response.send_message(embed=embed, ephemeral=True)
        else:
            to_guess = unidecode.unidecode(to_guess.lower())
            message = await interaction.channel.send(settings.HANGMAN[0])
            game = HangmanGame(to_guess=to_guess, message=message, author=interaction.user)
            await game.draw()
            self.games.append(game)
            logger.info(_('HANGMAN_LOG_GAME_CREATED') % {'user': interaction.user, 'word': to_guess})
            embed = Embed(title=_('HANGMAN_GAME_CREATED') % {'word': to_guess}, color=0x23a700)
            await interaction.response.send_message(embed=embed, ephemeral=True)    

    @app_commands.command()
    async def hangman_letter(self, interaction: Interaction, letter: str) -> None:
        await self.hangman_action(interaction, lambda game : game.test_letter(letter, interaction.user))

    @app_commands.command()
    async def hangman_guess(self, interaction: Interaction, word: str) -> None:
        await self.hangman_action(interaction, lambda game : game.test_word(word, interaction.user))

    @app_commands.command()
    async def hangman_stop(self, interaction: Interaction) -> None:
        await self.hangman_action(interaction, lambda game : game.stop(interaction.user))
