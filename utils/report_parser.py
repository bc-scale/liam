import json
import os
import time

from discord_webhook import DiscordWebhook, DiscordEmbed

REPORT_DIRECTORY = 'reports'
WEBHOOK_URL = 'https://discord.com/api/webhooks/990292046600359937/09DXC8bsM3g2UK9u2UKYwB6n_slvLgXRxhGrMx5lOyVCrNX12T7T_aMHR8quDHOIIBFT'

def read_vulnerabilities(data):

	vulnerabilities = []

	for vulnerability in data['vulnerabilities']:
		message = vulnerability['message']

		description = None
		if 'description' in vulnerability:
			description = vulnerability['description'][0:1020]
		file = vulnerability['location']['file']
		lines = vulnerability['location']['start_line']
		if 'end_line' in vulnerability['location']:
			lines = f"{vulnerability['location']['start_line']}-{vulnerability['location']['end_line']}"

		suffix = f'`{file}:{lines}`'
		message = message[0:256-len(suffix)-1]
		vulnerabilities.append({'message': message, 'description': description, 'suffix': suffix})
	return vulnerabilities

def read_report(file_path):

	vulnerabilities = []
	if os.path.isfile(file_path) and filename.endswith('.json'):
		with open(file_path, 'r') as file_handler:
			data = json.load(file_handler)			
			vulnerabilities += read_vulnerabilities(data)
	return vulnerabilities

if __name__ == '__main__':
	
	vulnerabilities = []
	if os.path.isdir(REPORT_DIRECTORY):
		files = os.listdir(REPORT_DIRECTORY)
		if files:
			for filename in files:
				vulnerabilities += read_report(os.path.join(REPORT_DIRECTORY, filename))


	for i in range(0, len(vulnerabilities), 5):
		sub_list = vulnerabilities[i:i+5]
		webhook = DiscordWebhook(url=WEBHOOK_URL)
		for vuln in sub_list:		
			embed = DiscordEmbed(title=f"{vuln['message']}{vuln['suffix']}", color='e24329')
			if vuln['description']:
				embed = DiscordEmbed(title=f"{vuln['message']}{vuln['suffix']}", description=vuln['description'], color='e24329')
			webhook.add_embed(embed)
		response = webhook.execute()
		time.sleep(1)
