#!/bin/sh
if ! [ -f "source/meta/" ]; then
	mkdir source/meta
fi
pylint ../src/ | pylint-json2html -f jsonextended -t report.j2 -o source/meta/pylint.rst
score=$(cat source/meta/pylint.rst | grep " / 10" | cut -d '/' -f1 | cut -d '`' -f2)
if [ -f "pylint.svg" ]; then
	rm pylint.svg
fi
anybadge -l pylint -v $score -f pylint.svg 2=red 4=orange 8=yellow 10=green