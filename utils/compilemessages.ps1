cd src
python manage.py mergetranslations --settings=liam.settings.development
python manage.py makemessages -l fr --settings=liam.settings.development
python manage.py compilemessages --settings=liam.settings.development
python manage.py splitcleantranslations --settings=liam.settings.development
cd ..
