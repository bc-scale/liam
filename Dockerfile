# pull the official base image
FROM python:3.10.1-alpine

RUN adduser -D -u 8135 runner

# set work directory
WORKDIR /code

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# copy project
COPY ./src/ /code
COPY ./requirements/ /code/requirements/

# install dependencies
RUN apk update && apk add git util-linux --no-cache \
	&& pip3 install --upgrade pip --no-cache-dir \
	&& pip3 install -r requirements/production.txt --no-cache-dir \
	&& mkdir -p /code/files/logs/ \
	&& chmod u+x /code/entrypoint_*.sh

STOPSIGNAL SIGTERM
EXPOSE 8000
