# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html
import os
import sys

import django
import sphinx_rtd_theme

sys.path.insert(0, os.path.abspath("../../src"))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Liam'
copyright = '2022, Imotekh'
author = 'Imotekh'
release = '1.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
	'sphinx.ext.autodoc',
	'sphinxcontrib_django2',
	'sphinx.ext.intersphinx',
	'sphinx_rtd_theme',
	'sphinx.ext.autosectionlabel',
	'sphinx.ext.viewcode',
	'autodocsumm',
]

templates_path = ['_templates']
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
pygments_style = 'monokai'

# -- Extensions options ------------------------------------------------------
# Django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'liam.settings.development')
django.setup()

intersphinx_mapping = {
	'python': ('https://docs.python.org/3', None),
	'django': ('http://docs.djangoproject.com/en/dev/', 'http://docs.djangoproject.com/en/dev/_objects/'),
}

autodoc_default_options = {
    'autosummary': True,
}
